/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.services.systemstatepropagator;

import java.util.Map;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import io.qaclana.api.Cached;
import io.qaclana.api.SystemState;
import io.qaclana.api.SystemStateContainer;
import io.qaclana.api.entity.event.SystemStateChange;

/**
 * @author Juraci Paixão Kröhling
 */
@WebListener
public class SystemStateOnBoot implements ServletContextListener {
    @Inject @Cached
    Map<String, String> cache;

    @Inject
    Event<SystemStateChange> systemStateChangeEvent;

    @Inject
    SystemStateContainer systemStateInstance;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String state = cache.get("system-state");
        SystemState systemState;
        if (null == state) {
            systemState = SystemState.DISABLED;
        } else {
            systemState = SystemState.valueOf(cache.get("system-state"));
        }
        systemStateChangeEvent.fire(new SystemStateChange(systemState));
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
