/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.proxy;

import org.jboss.logging.Logger;
import org.jboss.logging.annotations.Cause;
import org.jboss.logging.annotations.LogMessage;
import org.jboss.logging.annotations.Message;
import org.jboss.logging.annotations.MessageLogger;
import org.jboss.logging.annotations.ValidIdRange;

/**
 * @author Juraci Paixão Kröhling
 */
@MessageLogger(projectCode = "QCLN")
@ValidIdRange(min = 10360, max = 10379)
public interface MsgLogger {
    MsgLogger LOGGER = Logger.getMessageLogger(MsgLogger.class, MsgLogger.class.getPackage().getName());

    @LogMessage(level = Logger.Level.DEBUG)
    @Message(id = 10360, value = "Incoming request at Servlet.")
    void requestAtServlet();

    @LogMessage(level = Logger.Level.DEBUG)
    @Message(id = 10361, value = "Incoming request at Scheduler.")
    void requestAtScheduler();

    @LogMessage(level = Logger.Level.DEBUG)
    @Message(id = 10362, value = "Incoming request at Worker.")
    void requestAtWorker();

    @LogMessage(level = Logger.Level.DEBUG)
    @Message(id = 10363, value = "Adding header parameter [%s] with value [%s] to the outgoing request.")
    void addingHeader(String name, String value);

    @LogMessage(level = Logger.Level.DEBUG)
    @Message(id = 10364, value = "Adding request parameter [%s] with value [%s] to the outgoing request.")
    void addingParameter(String name, String value);

    @LogMessage(level = Logger.Level.WARN)
    @Message(id = 10365, value = "The HTTP method could not be determined. Method for the request: [%s].")
    void methodNotFound(String method);
}
