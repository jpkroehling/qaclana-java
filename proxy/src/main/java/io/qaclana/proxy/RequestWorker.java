/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.proxy;

import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.Callable;

import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.impl.client.HttpClients;

/**
 * @author Juraci Paixão Kröhling
 */
public class RequestWorker implements Callable<Void> {
    private static final MsgLogger logger = MsgLogger.LOGGER;
    private AsyncContext asyncContext;
    private String backingServer;
    private static HttpClient client = HttpClients
            .custom()
            .disableAuthCaching()
            .disableCookieManagement()
            .disableRedirectHandling()
            .build();

    public RequestWorker(AsyncContext asyncContext, String backingServer) {
        this.asyncContext = asyncContext;
        this.backingServer = backingServer;
    }

    @Override
    public Void call() throws Exception {
        logger.requestAtWorker();

        // N.B: this is mostly a workaround. The idea is to have something similar (or even using) Undertow's
        // Proxy client. It's performance is amazing, so, we should try to re-use it. For now, this will suffice.
        // This all means that this implementation is not covering all scenarios, not setting all required headers
        // and not handling corner cases. While the performance is not bad, it's also not what we want for the future.
        // On its current implementation (in debug mode, both on the proxy and on the target), this is what I could measure:
        // $ ab -c 1000 -n 100000 http://127.0.0.1:8280/example-no-filter-0.0.1-SNAPSHOT/v1/foo
        // Requests per second:    5902.64 [#/sec] (mean)
        // Without the proxy:
        // $ ab -c 1000 -n 100000 http://127.0.0.1:8380/example-no-filter-0.0.1-SNAPSHOT/v1/foo
        // Requests per second:    13631.11 [#/sec] (mean)

        HttpServletRequest incomingRequest = (HttpServletRequest) asyncContext.getRequest();
        HttpRequest outgoingRequest = getOutgoingRequest(incomingRequest);
        if (null == outgoingRequest) {
            asyncContext.complete();
            return null;
        }

        Collections.list(incomingRequest.getHeaderNames())
                .stream()
                .filter(name -> !"Content-Length".equalsIgnoreCase(name))
                .forEach(name -> Collections.list(incomingRequest.getHeaders(name))
                        .forEach(value -> {
                            logger.addingHeader(name, value);
                            outgoingRequest.addHeader(name, value);
                        }));
        outgoingRequest.addHeader("X-Forwarded-For", incomingRequest.getRemoteHost());

        if (outgoingRequest instanceof HttpEntityEnclosingRequestBase) {
            BasicHttpEntity entity = new BasicHttpEntity();
            entity.setContent(incomingRequest.getInputStream());
            ((HttpEntityEnclosingRequestBase) outgoingRequest).setEntity(entity);
        }

        URL backingServerUrl = new URL(backingServer);
        int port = backingServerUrl.getPort() == -1 ? backingServerUrl.getDefaultPort() : backingServerUrl.getPort();
        HttpHost host = new HttpHost(backingServerUrl.getHost(), port, backingServerUrl.getProtocol());
        HttpResponse incomingResponse;
        try {
            incomingResponse = client.execute(host, outgoingRequest);
        } catch (Exception e) {
            e.printStackTrace();
            asyncContext.complete();
            return null;
        }

        HttpServletResponse outgoingResponse = (HttpServletResponse) asyncContext.getResponse();
        outgoingResponse.setStatus(incomingResponse.getStatusLine().getStatusCode());
        Arrays.stream(incomingResponse.getAllHeaders()).forEach(header -> outgoingResponse.addHeader(header.getName(), header.getValue()));

        HttpEntity entity = incomingResponse.getEntity();
        if (null != entity) {
            entity.writeTo(outgoingResponse.getOutputStream());
        }

        asyncContext.complete();
        return null;
    }

    private HttpRequest getOutgoingRequest(HttpServletRequest request) {
        switch (request.getMethod().toUpperCase()) {
            case "DELETE": return new HttpDelete(request.getRequestURI());
            case "GET": return new HttpGet(request.getRequestURI());
            case "HEAD": return new HttpHead(request.getRequestURI());
            case "OPTIONS": return new HttpOptions(request.getRequestURI());
            case "PATCH": return new HttpPatch(request.getRequestURI());
            case "POST": return new HttpPost(request.getRequestURI());
            case "PUT": return new HttpPut(request.getRequestURI());
            default: {
                logger.methodNotFound(request.getMethod());
                return null;
            }
        }
    }
}
