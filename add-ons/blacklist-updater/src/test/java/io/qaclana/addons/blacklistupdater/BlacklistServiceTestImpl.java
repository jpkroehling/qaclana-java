/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.addons.blacklistupdater;

import io.qaclana.api.control.BlacklistService;
import io.qaclana.api.entity.IpRange;

import javax.ejb.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Juraci Paixão Kröhling
 */
@Singleton
public class BlacklistServiceTestImpl implements BlacklistService {
    List<IpRange> ipRangeList = new ArrayList<>();

    @Override
    public List<IpRange> list() {
        return ipRangeList;
    }

    @Override
    public void add(IpRange ipRange) {
        ipRangeList.add(ipRange);
    }

    @Override
    public void remove(IpRange ipRange) {
        ipRangeList.remove(ipRange);
    }

    @Override
    public boolean isInBlacklist(IpRange ipRange) {
        return ipRangeList.contains(ipRange);
    }

    List<IpRange> getIpRangeList() {
        return Collections.unmodifiableList(ipRangeList);
    }
}
